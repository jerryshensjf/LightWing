package com.javaforever.lightwing.gui;

import java.awt.Image;
import java.awt.event.ActionEvent;

/**
 * This program is written by Jerry Shen(Shen Ji Feng) use the technology of
 * SWING GUI and the OO design
 * 
 * @author Jerry Shen all rights reserved.
 * Email:jerry_shen_sjf@qq.com
 * Please report bug to this email.
 * Open source under GPLv2
 * 
 * version 0.7.12
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class AboutFrame extends JFrame implements MouseListener {
	private JPanel aboutPane;

	private JLabel msg;

	private JLabel msg0;

	private JLabel msg1;

	private JLabel msg2;

	private JLabel msg3;
	
	private JLabel msg4;

	private JButton exit;

	private ImageIcon projectIcon;

	private JLabel projectLabel;

	public AboutFrame(String strName) throws Exception {
		super(strName);
		setSize(700, 700);
		setResizable(false);
		String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
		// UIManager.setLookAndFeel(windows);
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));

		aboutPane = new JPanel();
		projectLabel = new JLabel();
		File directory = new File(".");
		String rootFolder = directory.getAbsolutePath();
		rootFolder = rootFolder.substring(0, rootFolder.length() - 2);
		String picPath0 =  	"images/light.jpg";
		String picPath = rootFolder + "/src/images/light.jpg";
		picPath0 = picPath0.replace("//", "/");
		picPath = picPath.replace("//", "/");
		File fsrc = new File(picPath);
		if (!fsrc.exists()) {
			System.out.println(picPath0);
			Resource res = new Resource();
			Image img = res.getImagePath(picPath0);
			projectIcon = new ImageIcon(img);
			projectIcon.setImage(projectIcon.getImage().getScaledInstance(640, 480, Image.SCALE_DEFAULT));
			projectLabel.setIcon(projectIcon);
		} else{
			System.out.println(picPath);
			projectIcon = new ImageIcon(picPath);
			projectIcon.setImage(projectIcon.getImage().getScaledInstance(640, 480, Image.SCALE_DEFAULT));
			projectLabel.setIcon(projectIcon);
		}

		msg = new JLabel(
				"                                                                光之翼Java通用代码生成器                                                                ");
		msg0 = new JLabel(
				"                                                                作者：沈戟峰 jerry_shen_sjf@qq.com                                                                ");
		msg1 = new JLabel(
				"                                                                希望您喜欢！                                                ");
		msg2 = new JLabel(
				"                                                                版本：2.2.0                                                ");
		msg3 = new JLabel(
				"                                                                      发布日期: 2021-6-8                                                              ");
		msg4 = new JLabel("                                                                   交流QQ群：277689737                                                              　     ");
		exit = new JButton("    关   闭    ");
		exit.addMouseListener(this);
		aboutPane.add(projectLabel);
		aboutPane.add(msg);
		aboutPane.add(msg0);
		aboutPane.add(msg1);
		aboutPane.add(msg2);
		aboutPane.add(msg3);
		aboutPane.add(msg4);
		aboutPane.add(exit);

		setContentPane(aboutPane);
		setLocationRelativeTo(null);// 窗口在屏幕中间显示

	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {
		this.setVisible(false);
	}

	public void mousePressed(MouseEvent e) {
		// System.out.println("Jerry Press");

	}

	public void mouseReleased(MouseEvent e) {
		// System.out.println("Jerry Release");
	}

	public void mouseExited(MouseEvent e) {
		// System.out.println("Jerry Exited");

	}

	public void mouseEntered(MouseEvent e) {
		// System.out.println("Jerry Entered");

	}

	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == exit) {
				this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			}
		} catch (Exception ie) {
			ie.printStackTrace();
			;
		}
	}

	public static void main(String[] args) throws Exception {
		AboutFrame about = new AboutFrame("About");
		about.setVisible(true);
	}
}
