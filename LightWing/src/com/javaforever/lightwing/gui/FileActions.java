package com.javaforever.lightwing.gui;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.File;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.swing.filechooser.*;

public class FileActions {
	public String fileExtension = "";
	public String fileName = "";

	// static final long serialVersionUID = 3228193999994336070L;
	// 打开文件
	String fileOpen() {

		// 设置普通参数
		BufferedReader in = null; // 设置读缓冲
		JFrame dialogFrame = new JFrame(); // 弹出窗口
		String filePath; // 文件路径
		StringBuffer sbTemp = new StringBuffer(""); // 文件缓冲
		String sTemp = null; // 行缓冲

		// 设置打开文件的扩展名 开始
		JFileChooser c = new JFileChooser();
		MyFileFilter filter = new MyFileFilter();
		filter.addExtension("sgs");
		filter.setDescription("Sgs文件");
		c.setFileFilter(filter);
		// 设置打开文件的扩展名 结束
		int rVal = c.showOpenDialog(dialogFrame);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			filePath = c.getSelectedFile().getPath();
			this.fileName = filePath;
			try { // try ,catch 抛出，捕获 异常
				in = new BufferedReader(new FileReader(filePath)); // 文件路径
				fileExtension = filter.getExtension(new File(filePath));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace(); // 抛出异常时执行的代码
			}

			try {
				while ((sTemp = in.readLine()) != null) { // sTemp行缓冲
					sbTemp.append(sTemp + "\n"); // sbTemp文件缓冲
				}
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e3) {
				e3.printStackTrace();
			}
			
		}
		
		return (sbTemp.toString());
	}

	String fileSaveAs() {
		// 设置普通参数
		JFrame dialogFrame = new JFrame(); // 弹出窗口
		StringBuffer filePath = new StringBuffer(""); // 文件路径

		// 设置保存文件的扩展名 开始
		JFileChooser c = new JFileChooser();
		MyFileFilter filter = new MyFileFilter();
		filter.addExtension("sgs");
		filter.setDescription("Sgs文件");
		c.setFileFilter(filter);
		// 设置保存文件的扩展名 结束
		int rVal = c.showSaveDialog(dialogFrame);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			filePath.append(c.getSelectedFile().getPath());
		}

		/*
		 * 判断: 如果保存文件的时候没有指示文件扩展名，则自动指示为.sgs。 如果保存文件的时候已经指示了文件扩展，则以指定方式保存。
		 */
		if ((filePath.lastIndexOf(".")) == -1) {
			filePath.append(".sgs");
			return (filePath.toString());
		} else
			return (filePath.toString());
	}

	int fileNotSaveMessage() {
		int i;
		i = JOptionPane.showConfirmDialog(null, "文件已经更改,想保存文件吗?", "记事本", JOptionPane.YES_NO_OPTION);
		return i;
	}
}

class MyFileFilter extends FileFilter {
	private static String TYPE_UNKNOWN = "Type Unknown";
	private static String HIDDEN_FILE = "Hidden File";
	private Hashtable filters = null;
	private String description = null;
	private String fullDescription = null;
	private boolean useExtensionsInDescription = true;

	public MyFileFilter() {
		this.filters = new Hashtable();
	}

	public MyFileFilter(String extension) {
		this(extension, null);
	}

	public MyFileFilter(String extension, String description) {
		this();
		if (extension != null)
			addExtension(extension);
		if (description != null)
			setDescription(description);
	}

	public MyFileFilter(String[] filters) {
		this(filters, null);
	}

	public MyFileFilter(String[] filters, String description) {
		this();
		for (int i = 0; i < filters.length; i++) {
			// add filters one by one
			addExtension(filters[i]);
		}
		if (description != null)
			setDescription(description);
	}

	public boolean accept(File f) {
		if (f != null) {
			if (f.isDirectory()) {
				return true;
			}
			String extension = getExtension(f);
			if (extension != null && filters.get(getExtension(f)) != null) {
				return true;
			}
			;
		}
		return false;
	}

	public String getExtension(File f) {
		if (f != null) {
			String filename = f.getName();
			int i = filename.lastIndexOf('.');
			if (i > 0 && i < filename.length() - 1) {
				return filename.substring(i + 1).toLowerCase();
			}
			;
		}
		return null;
	}

	public void addExtension(String extension) {
		if (filters == null) {
			filters = new Hashtable(5);
		}
		filters.put(extension.toLowerCase(), this);
		fullDescription = null;
	}

	public String getDescription() {
		if (fullDescription == null) {
			if (description == null || isExtensionListInDescription()) {
				fullDescription = description == null ? "(" : description + " (";
				// build the description from the extension list
				Enumeration extensions = filters.keys();
				if (extensions != null) {
					fullDescription += "." + (String) extensions.nextElement();
					while (extensions.hasMoreElements()) {
						fullDescription += ", ." + (String) extensions.nextElement();
					}
				}
				fullDescription += ")";
			} else {
				fullDescription = description;
			}
		}
		return fullDescription;
	}

	public void setDescription(String description) {
		this.description = description;
		fullDescription = null;
	}

	public void setExtensionListInDescription(boolean b) {
		useExtensionsInDescription = b;
		fullDescription = null;
	}

	public boolean isExtensionListInDescription() {
		return useExtensionsInDescription;
	}
}