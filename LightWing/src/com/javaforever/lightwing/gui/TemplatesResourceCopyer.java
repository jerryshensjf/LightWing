package com.javaforever.lightwing.gui;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.javaforever.gatescore.core.FileCopyer;
import org.light.domain.Project;
import org.light.utils.StringUtil;

public class TemplatesResourceCopyer {
	// 下载并解压拷贝的文件
	public void downloadUnpackTemplates(String projectFolderPath) throws Exception{
		 Resource res = new Resource();
		 String myres = new File(".").getAbsolutePath();
		 myres = myres.substring(0,myres.length()-2)+"/templates/FrontEndTemplates.zip";
		 System.out.println("JerryDebug:myres:"+myres);
		 File myResF = Paths.get(myres).toFile();
		 if (myResF.exists()) {
			 File target = new File(projectFolderPath+"/FrontEndTemplates.zip");
			 Files.copy(myResF.toPath(), target.toPath());
		 }else {		 
			 res.downloadRes("/templates/FrontEndTemplates.zip", projectFolderPath+"/FrontEndTemplates.zip");
		 }
		 ZipCompressor compressor = new ZipCompressor();
		 compressor.unZipFilesAndDeleteOrginal(projectFolderPath+"/FrontEndTemplates.zip", projectFolderPath);
	}
	
	public void downloadUnpackBackEndTemplates(String projectFolderPath,boolean useOracle) throws Exception{
		 Resource res = new Resource();
		 String myres = new File(".").getAbsolutePath();
		 myres = myres.substring(0,myres.length()-2)+"/templates/BackEndTemplates.zip";
		 System.out.println("JerryDebug:myres:"+myres);
		 File myResF = Paths.get(myres).toFile();
		 if (myResF.exists()) {
			 File target = new File(projectFolderPath+"/BackEndTemplates.zip");
			 Files.copy(myResF.toPath(), target.toPath());
		 }else {		 
			 res.downloadRes("/templates/BackEndTemplates.zip", projectFolderPath+"/BackEndTemplates.zip");
		 }
		 ZipCompressor compressor = new ZipCompressor();
		 compressor.unZipFilesAndDeleteOrginal(projectFolderPath+"/BackEndTemplates.zip", projectFolderPath+"/backendtemplates/");
	
		FileCopyer copy = new FileCopyer();
		
		if (useOracle) {
			File libfrom = new File(projectFolderPath+"backendtemplates/" + "lib/");
			File libto = new File(projectFolderPath + "src/main/webapp/WEB-INF/lib/");
			
			copy.dirFrom = libfrom;
			copy.dirTo = libto;
			copy.listFileInDir(libfrom);
		}
			
		File cssfrom = new File(projectFolderPath+"backendtemplates/" + "css/");
		File cssto = new File(projectFolderPath + "src/main/resources/static/css/");

		copy.dirFrom = cssfrom;
		copy.dirTo = cssto;
		copy.listFileInDir(cssfrom);
		
		File jsfrom = new File(projectFolderPath+"backendtemplates/" + "js/");
		File jsto = new File(projectFolderPath + "src/main/resources/static/js/");
		 		
		copy.dirFrom = jsfrom;
		copy.dirTo = jsto;
		copy.listFileInDir(jsfrom);
		
		File easyuifrom = new File(projectFolderPath+"backendtemplates/" + "easyui/");
		File easyuito = new File(projectFolderPath + "src/main/resources/static/easyui/");

		copy.dirFrom = easyuifrom;
		copy.dirTo = easyuito;
		copy.listFileInDir(easyuifrom);
		
		JarBackendProjectExportUtil.delFolder(projectFolderPath+"backendtemplates/" );
	}
	
	public void downloadUnpackSMEUBackEndTemplates(Project project, String projectFolderPath, boolean useOracle)
			throws Exception {
		Resource res = new Resource();
		String myres = new File(".").getAbsolutePath();
		myres = myres.substring(0, myres.length() - 2) + "/templates/BackEndTemplates.zip";
		System.out.println("JerryDebug:myres:" + myres);
		File myResF = Paths.get(myres).toFile();
		if (myResF.exists()) {
			File target = new File(projectFolderPath + "/BackEndTemplates.zip");
			Files.copy(myResF.toPath(), target.toPath());
		} else {
			res.downloadRes("/templates/BackEndTemplates.zip", projectFolderPath + "/BackEndTemplates.zip");
		}
		ZipCompressor compressor = new ZipCompressor();
		compressor.unZipFilesAndDeleteOrginal(projectFolderPath + "/BackEndTemplates.zip",
				projectFolderPath + "/backendtemplates/");

		FileCopyer copy = new FileCopyer();
		String templateFolderPath =projectFolderPath + "/backendtemplates/";

		if (project.getDbType().equalsIgnoreCase("oracle")) {
			if (!"msmeu".equalsIgnoreCase(project.getTechnicalstack())) {
				File smeulibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
				File smeulibfrom = new File(templateFolderPath + "smeulib/");

				// 设置来源去向
				copy.dirFrom = smeulibfrom;
				copy.dirTo = smeulibto;
				copy.listFileInDir(smeulibfrom);
			}

			if (true) {
				File cssfrom = new File(templateFolderPath + "css/");
				File cssto = new File(projectFolderPath + "WebContent/css/");

				copy.dirFrom = cssfrom;
				copy.dirTo = cssto;
				copy.listFileInDir(cssfrom);

				File jsfrom = new File(templateFolderPath + "js/");
				File jsto = new File(projectFolderPath + "WebContent/js/");

				copy.dirFrom = jsfrom;
				copy.dirTo = jsto;
				copy.listFileInDir(jsfrom);

				File easyuifrom = new File(templateFolderPath + "easyui/");
				File easyuito = new File(projectFolderPath + "WebContent/easyui/");

				copy.dirFrom = easyuifrom;
				copy.dirTo = easyuito;
				copy.listFileInDir(easyuifrom);

				File uploadjsfrom = new File(templateFolderPath + "uploadjs/");
				File uploadjsto = new File(projectFolderPath + "WebContent/uploadjs/");

				copy.dirFrom = uploadjsfrom;
				copy.dirTo = uploadjsto;
				copy.listFileInDir(uploadjsfrom);
			}
		} else {
			if (!"msmeu".equalsIgnoreCase(project.getTechnicalstack())) {
				File smeulibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
				File smeulibfrom = new File(templateFolderPath + "smeulib/");

				// 设置来源去向
				copy.dirFrom = smeulibfrom;
				copy.dirTo = smeulibto;
				copy.listFileInDir(smeulibfrom);

				if (StringUtil.isBlank(project.getDbType()) || "mariadb".equalsIgnoreCase(project.getDbType())) {
					File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File libfrom = new File(templateFolderPath + "mariadblib/");
					// 设置来源去向
					copy.dirFrom = libfrom;
					copy.dirTo = libto;
					copy.listFileInDir(libfrom);
				} else if ("mysql".equalsIgnoreCase(project.getDbType())) {
					File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File libfrom = new File(templateFolderPath + "mysql8lib");
					// 设置来源去向
					copy.dirFrom = libfrom;
					copy.dirTo = libto;
					copy.listFileInDir(libfrom);
				} else if ("postgresql".equalsIgnoreCase(project.getDbType())
						|| "pgsql".equalsIgnoreCase(project.getDbType())) {
					File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File libfrom = new File(templateFolderPath + "pgsqllib/");
					// 设置来源去向
					copy.dirFrom = libfrom;
					copy.dirTo = libto;
					copy.listFileInDir(libfrom);
				}
			}

			if (true) {
				File cssfrom = new File(templateFolderPath + "css/");
				File cssto = new File(projectFolderPath + "WebContent/css/");

				copy.dirFrom = cssfrom;
				copy.dirTo = cssto;
				copy.listFileInDir(cssfrom);

				File jsfrom = new File(templateFolderPath + "js/");
				File jsto = new File(projectFolderPath + "WebContent/js/");

				copy.dirFrom = jsfrom;
				copy.dirTo = jsto;
				copy.listFileInDir(jsfrom);

				File easyuifrom = new File(templateFolderPath + "easyui/");
				File easyuito = new File(projectFolderPath + "WebContent/easyui/");

				copy.dirFrom = easyuifrom;
				copy.dirTo = easyuito;
				copy.listFileInDir(easyuifrom);

				File uploadjsfrom = new File(templateFolderPath + "uploadjs/");
				File uploadjsto = new File(projectFolderPath + "WebContent/uploadjs/");

				copy.dirFrom = uploadjsfrom;
				copy.dirTo = uploadjsto;
				copy.listFileInDir(uploadjsfrom);
			}
		}
		JarBackendProjectExportUtil.delFolder(projectFolderPath + "backendtemplates/");		
	}
}