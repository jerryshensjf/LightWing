# 光之翼Java通用代码生成器

### 项目图片
![输入图片说明](https://images.gitee.com/uploads/images/2020/1224/192441_a5f777a7_1203742.jpeg "light.jpg")

#### 介绍
光之翼Java通用代码生成器，Swing界面，采用光和时空之门最新前端后端生成器引擎。是动词算子式代码生成器的封笔之作。

### 介绍视频地址
光之翼2.2.0 Beta4版介绍视频，B站：[https://www.bilibili.com/video/BV1Vq4y177d1/](https://www.bilibili.com/video/BV1Vq4y177d1/)

1.0.0版本B站地址：
[https://www.bilibili.com/video/BV1d5411H77j](https://www.bilibili.com/video/BV1d5411H77j)

#### 软件架构

后端：EasyUI,SpringBoot,SpringMVC,Spring,MyBatis,MariaDB/Oracle

前端：Vue,ElementUI,Node.js

### 新版本公布
光之翼java通用代码生成器2.2.0 Beta6　智慧光之翼版本公布。为Swing独立版，采用光2.2.0Beta7智慧版本最新后端生成引擎,时空之门4.6.0 Beta4版最新前端生成引擎。

由于某种原因Eclipse导出的jar，在Ｗindows上运行双击是启动不了的，您需要在命令窗口使用java -jar LightWing_2_2_0_beta6.jar启动。

请去[https://gitee.com/jerryshensjf/GatesCore/attach_files](https://gitee.com/jerryshensjf/GatesCore/attach_files)　下载


### 界面截图
![界面截图](https://images.gitee.com/uploads/images/2020/1227/213049_0c36950d_1203742.png "lightwing_soft.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/211521_2a4fa98b_1203742.png "lightWing_about.png")


### 运行方式

1. 启动jar
1. 下载示例的Excel模板
1. 根据需要修改模板
1. 上传模板
1. 单击“代码生成”按钮
1. 在jar所在的目录下有代码生成物的zip压缩包

### 编译错与编译警告

光之翼1.0.0 Beta2版增强了编译错与编译警告，如图所示：

编译错

请使用DenyFieldsSample2示例，效果如图所示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0116/134351_a5c49434_1203742.png "compileError.png")

编译警告

请使用DenyFieldsCorrect2示例，并取消对“忽略编译警告”选框的选择。效果如图所示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0116/134429_01466145_1203742.png "compileWarning.png")

### 交流QQ群
动词算子式式代码生成器群  277689737
